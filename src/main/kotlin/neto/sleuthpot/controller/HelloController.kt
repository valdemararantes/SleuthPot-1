package neto.sleuthpot.controller

import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.Request
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject

@RestController
@RequestMapping("hello")
class HelloController(private val restTemplate: RestTemplate,
                      private val okHttpClient: OkHttpClient) {

    private val uri = "http://localhost:8080/hello"
    private val log = LoggerFactory.getLogger(javaClass)

    @GetMapping
    fun hello() = "Hello world"

    @GetMapping("slow")
    fun slowHello() = Thread.sleep(5000).run { "Slow Hello" }

    @GetMapping("call-hello")
    fun callHelloUsingREST(): String? {
        return restTemplate.getForObject("http://localhost:8080/hello")
    }

    @GetMapping("call")
    fun call(@RequestParam("method") method: String = "slow", @RequestParam("useOkHttp") useOkHttp: Boolean = true): String? {
        log.info("Arguments = [method = $method, useOkHttp = $useOkHttp]")
        val url = "$uri/$method"
        return if (useOkHttp) {

            okHttpClient.apply {
                log.info("OkHttpClient timeouts:\n" +
                        "[writeTimeoutMillis = ${writeTimeoutMillis()},\n" +
                        "readTimeoutMillis = ${readTimeoutMillis()},\n" +
                        "connectTimeoutMillis = ${connectTimeoutMillis()}")
            }
            val request = Request.Builder()
                    .get().url(url)
                    .addHeader("Authorization", Credentials.basic("user", "pass"))
                    .build()

            okHttpClient.newCall(request).execute().use { response ->
                return response.body()?.string()
            }
        } else {
            restTemplate.getForObject(url)
        }
    }
}
