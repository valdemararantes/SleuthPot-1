package neto.sleuthpot.config

import ch.qos.logback.access.servlet.TeeFilter
import ch.qos.logback.classic.helpers.MDCInsertingServletFilter
import neto.sleuthpot.filter.UserServletFilter
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.servlet.Filter

@Configuration
class LogFiltersConfig {
    @Bean(name = ["mdcFilter"])
    fun mdcFilter(): Filter {
        return MDCInsertingServletFilter()
    }

    @Bean(name = ["teeFilter"])
    fun teeFilter(): Filter {
        return TeeFilter()
    }

    @Bean(name = ["userFilter"])
    fun userFilter(): Filter {
        return UserServletFilter()
    }

}
